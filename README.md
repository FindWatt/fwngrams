### FwNGrams ###

* This project is for memory efficient counting and storing of ngrams across multiple categories

### Install ###

* pip install git+https://bitbucket.org/FindWatt/ngrams

### Example usage ###

Creating the class

* a_texts = ["This is a function test", "Testing this new class", "This class is a test"]

* a_fields = ["Testing", "Classes"]

* a_cats = [["Test", "Both", "Test"], ["Both", "Class", "Class"]]

* o_ng = NGramGroups(a_texts, a_fields, a_cats)

Get the counts of ngram appearances in any item and in a particular category

* o_ng.ngram_total(('class',))

* o_ng.ngram_count(('class', 'test'), category="Test", field="Tests")

Get a frequency dictionary of the categories that ngram appears in

* o_ng.ngram_dict(('class',))

Get the name of an ngram by index

* o_ng.ngram_by_index(8)

Get the herfindahl (the distribution spread) of an ngram

* o_ng.ngram_herfindahl(('class',))

* o_ng.ngram_herfindahl(('class', 'test'))

Get a labelled frequency dictionary of ngrams in a specific category and field

* o_ng.category_dict('Both', 'Classes')

Get a labelled frequency dictionary of ngrams in a specific category in any field

* o_ng.category_dict('Test', '<any>')


Updating the class with more ngrams

* a_new_texts = ["This is an update test", "Testing this new class", "This class is updated"]

* a_new_fields = ["Testing", "Updates"]

* a_new_cats = [["Test", "Both", "Test"], ["Update", "Class", "Update"]]

* o_ng.update(a_new_texts, a_new_fields, a_new_cats)

### Input Formats ###

raw titles

inputs texts = [
	"title 1",
	"title 2",
	"title 3",
	...
]

tokenized titles

inputs texts = [
	["title", "1"],
	["title", "2"],
	["title", "3"],
	...
]

row ngrams

inputs texts = [
	[("row1", "ngrams"), ("row1",), ("ngrams",)],
	[("row2", "ngrams"), ("row2",), ("ngrams",)],
	[("row3", "ngrams"), ("row3",), ("ngrams",)],
]