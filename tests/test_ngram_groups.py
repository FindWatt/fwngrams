﻿'''
Test cases for FwNGrams/ngram_groups
'''
from __future__ import absolute_import
import math
from collections import OrderedDict
from FwNGrams import NGramGroups

class TestNGramGroups():
    a_texts = ["This is a function test", "Testing this new class", "This class is a test"]
    a_fields = ["Testing", "Classes"]
    a_cats = [["Test", "Both", "Test"], ["Both", "Class", "Class"]]

    ng = NGramGroups(a_texts, a_fields, a_cats)
    
    # test indexes
    def test_fields(self):
        assert self.ng.field_names == ['Testing', 'Classes']
        assert self.ng.fields == {'<all>': 0, 'Testing': 1, 'Classes': 2}
        assert self.ng.idx_fields == {0: '<all>', 1: 'Testing', 2: 'Classes'}
    
    def test_categories(self):
        assert self.ng.categories == {
            0: {'<all>': 0, 'Test': 1, 'Both': 3, 'Class': 6},
            1: {'Test': 2, 'Both': 5},
            2: {'Both': 4, 'Class': 7},
        }
        assert self.ng.idx_categories == {
            0: {0: '<all>', 1: 'Test', 3: 'Both', 6: 'Class'},
            1: {2: 'Test', 5: 'Both'},
            2: {4: 'Both', 7: 'Class'},
        }

    def test_ngrams(self):
        assert self.ng.ngrams == OrderedDict(
            [(('function', 'test'), 0),
             (('function',), 1),
             (('test',), 2),
             (('testing', 'new', 'class'), 3),
             (('testing', 'new'), 4),
             (('new', 'class'), 5),
             (('testing',), 6),
             (('new',), 7),
             (('class',), 8),
             (('class', 'test'), 9)]
        )

    # test overall counts
    def test_category_counts(self):
        assert self.ng._category_counts == {
        (0, 0): {0: 1, 1: 1, 2: 2, 3: 1, 4: 1, 5: 1, 6: 1, 7: 1, 8: 2, 9: 1},
        (0, 1): {0: 1, 1: 1, 2: 2, 8: 1, 9: 1},
        (0, 3): {0: 1, 1: 1, 2: 1, 3: 1, 4: 1, 5: 1, 6: 1, 7: 1, 8: 1},
        (0, 6): {2: 1, 3: 1, 4: 1, 5: 1, 6: 1, 7: 1, 8: 2, 9: 1},
        (1, 2): {0: 1, 1: 1, 2: 2, 8: 1, 9: 1},
        (1, 5): {3: 1, 4: 1, 5: 1, 6: 1, 7: 1, 8: 1},
        (2, 4): {0: 1, 1: 1, 2: 1},
        (2, 7): {2: 1, 3: 1, 4: 1, 5: 1, 6: 1, 7: 1, 8: 2, 9: 1}
    }
 
    def test_ngram_counts(self):
        assert self.ng._ngram_counts == {
            0: {0: {0: 1, 1: 1, 3: 1}, 1: {2: 1}, 2: {4: 1}},
            1: {0: {0: 1, 1: 1, 3: 1}, 1: {2: 1}, 2: {4: 1}},
            2: {0: {0: 2, 1: 2, 3: 1, 6: 1}, 1: {2: 2}, 2: {4: 1, 7: 1}},
            3: {0: {0: 1, 3: 1, 6: 1}, 1: {5: 1}, 2: {7: 1}},
            4: {0: {0: 1, 3: 1, 6: 1}, 1: {5: 1}, 2: {7: 1}},
            5: {0: {0: 1, 3: 1, 6: 1}, 1: {5: 1}, 2: {7: 1}},
            6: {0: {0: 1, 3: 1, 6: 1}, 1: {5: 1}, 2: {7: 1}},
            7: {0: {0: 1, 3: 1, 6: 1}, 1: {5: 1}, 2: {7: 1}},
            8: {0: {0: 2, 1: 1, 3: 1, 6: 2}, 1: {2: 1, 5: 1}, 2: {7: 2}},
            9: {0: {0: 1, 1: 1, 6: 1}, 1: {2: 1}, 2: {7: 1}}
        }

    # test results for individual ngrams
    def test_ngram_by_index(self):
        assert self.ng.ngram_by_index(8) == ('class',)
        assert self.ng.ngram_by_index(10) is None
    
    def test_ngram_total(self):
        assert self.ng.ngram_total(('class',)) == 2
        assert self.ng.ngram_total(2) == 2
    
    def test_ngram_count(self):
        assert self.ng.ngram_count(('test',), '<all>') == 2
        assert self.ng.ngram_count(2, '') == 2
        assert self.ng.ngram_count(2, 'Both') == 1
    
    def test_ngram_dict(self):
        ''' all the fields and categories that the ngram appears in
            or freq dict of the categories that the ngram appears in '''
        assert self.ng.ngram_dict(('class',)) == {
            '<all>': {'<all>': 2, 'Both': 1, 'Class': 2, 'Test': 1},
            'Classes': {'Class': 2},
            'Testing': {'Both': 1, 'Test': 1}
        }
        assert self.ng.ngram_dict(('class',), field='Testing') == {
            'Both': 1, 'Test': 1
        }

    def test_ngram_herfindahl(self):
        assert math.isclose(self.ng.ngram_herfindahl(('class',), '<all>'), 0.375)
        assert math.isclose(self.ng.ngram_herfindahl(8, ''), 0.375)
        assert math.isclose(self.ng.ngram_herfindahl(('class',), 'Testing'), 0.5)

    # test results for individual fields and categories
    def test_field_dict_all(self):
        e_all = self.ng.field_dict(field='<all>')
        assert '<all>' in e_all
        assert 'Class' in e_all
        assert 'Test' in e_all
        assert 'Both' in e_all
        assert e_all['<all>'][('class',)] == 2
        assert e_all['Class'][('class',)] == 2
        assert e_all['Test'][('function', 'test')] == 1
        
    def test_field_dict_single(self):
        e_testing = self.ng.field_dict(field='Testing')
        assert 'Both' in e_testing
        assert 'Test' in e_testing
        assert e_testing['Test'][('test', )] == 2
        assert e_testing['Both'][('new',)] == 1
        
    def test_category_dict(self):
        e_field_all = self.ng.field_dict(field='<all>')['<all>']
        e_category_all = self.ng.category_dict(field='<all>')
        assert e_field_all == e_category_all
        assert e_category_all == self.ng.category_dict(category='<all>', field='<all>')
        
    def test_category_dict_single(self):
        e_cat = self.ng.category_dict(category="Both", field="Classes")
        assert e_cat == {
            ('function',): 1, ('function', 'test'): 1, ('test',): 1
        }
