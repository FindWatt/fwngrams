import os

import setuptools
from setuptools.command.install import install

module_path = os.path.join(os.path.dirname(__file__), 'FwNGrams/__init__.py')
version_line = [line for line in open(module_path)
                if line.startswith('__version__')][0]

__version__ = version_line.split('__version__ = ')[-1][1:][:-2]

        

setuptools.setup(
    name="FwNGrams",
    version=__version__,
    url="https://bitbucket.org/FindWatt/fwngrams",

    author="FindWatt",

    description='''
    This project is for memory efficient counting and storing of ngrams across multiple categories
    ''',
    long_description=open('README.md').read(),

    packages = setuptools.find_packages(),
    py_modules=['FwNGrams'],
    zip_safe=False,
    platforms='any',
    dependency_links=[
        "https://bitbucket.org/FindWatt/fwutil/get/master.tar.gz#egg=FwUtil-999.0.0",
        "https://bitbucket.org/FindWatt/fwmp/get/master.tar.gz#egg=FwMP-999.0.0",
    ],
    install_requires = [
        'FwUtil',
        'FwMP', 
    ]
)
