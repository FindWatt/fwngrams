﻿from collections import OrderedDict
from itertools import islice
import pandas as pd
import numpy as np
from FwUtil.FwText import all_ngrams_tuples
from FwUtil.FwFreq import herfindahl, herfindahl_freqs_dict
from FwMP.text_lists import text_to_ngrams


class NGramGroups():
    ''' This class is to store the relationships between
        ngrams and one or more groups in a fast, memory-efficient way.
        
        There will be dictionaries storing the counts by int references,
        a dict of ngram tuple/str to ngram indexes,
        and a dict of category str to cat indexes
    '''
    
    def __init__(self, a_texts=[], a_fields=[], aa_categories=[],
                 i_max_size=6, i_min_size=1,
                 stop_words="english", remove_punctuation=True,
                 b_case_insensitive=True):
        ''' @description: create the ngram index
            @arg a_texts: {list} a list of strings to split into ngrams.
                          This is only used if a_ngrams is missing
            @arg aa_cats: {list} a list containing one or more groups lists
            @arg aa_cat_names: {list} a list containing the names of the categories
        '''
        self._max_size, self._min_size = i_max_size, i_min_size
        self._stop_words = stop_words
        self._remove_punctuation = remove_punctuation
        self._case_insensitive = b_case_insensitive

        # dicts mapping ngrams and categories to their respective indexes
        self.field_names = a_fields
        
        self.fields, self.idx_fields = {}, {}
        
        self.categories, self.idx_categories = {}, {}
        
        self.ngrams = OrderedDict()
        
        self._ngram_count, self._category_count, self._field_count = 0, 0, 0
        self._ngram_counts, self._category_counts = {}, {}
        
        # per row lists of ngrams and categories
        self.row_ngrams = []
        self.row_categories = []
        
        # create indexes and turn inputs into list of integers
        self._create_category_indexes(a_fields, aa_categories)
        self._create_ngram_indexes(a_texts)
        
        # count ngrams per category
        self._create_count_dicts()
    
    # create the initial dictionaries and counts
    def _create_category_indexes(self, a_fields, aa_categories):
        ''' turn lists of categories into list of category_indexes, and
            create dicts of category: index
        '''
        self._field_count = 1 + len(a_fields)
        self.fields = {'<all>': 0}
        self.idx_fields = {0: '<all>'}
        self.fields.update({field: idx + 1 for idx, field in enumerate(a_fields)})
        self.idx_fields.update({idx + 1: field for idx, field in enumerate(a_fields)})
        
        self.categories = {idx: {} for idx in self.idx_fields}
        self.idx_categories = {idx: {} for idx in self.idx_fields}
        
        self.categories[0]['<all>'] = 0
        self.idx_categories[0][0] = '<all>'
        
        i_category = 1
        self.row_categories = []
        ai_fields = [i for i in range(1, self._field_count)]
        for categories in zip(*aa_categories):
            a_row = []
            for i_field, cat in zip(ai_fields, categories):
                if cat not in self.categories[0]:
                    self.categories[0][cat] = i_category
                    self.idx_categories[0][i_category] = cat
                    i_category += 1
                a_row.append((0, self.categories[0][cat]))
                    
                if cat not in self.categories[i_field]:
                    self.categories[i_field][cat] = i_category
                    self.idx_categories[i_field][i_category] = cat
                    i_category += 1
                a_row.append((i_field, self.categories[i_field][cat]))
            
            self.row_categories.append(a_row)
        self._category_count = i_category
    
    def _create_ngram_indexes(self, a_texts):
        ''' turn list of texts into list of ngram_indexes, and
            create dict of ngram: index
        '''
        self.ngrams, self.row_ngrams = OrderedDict(), []
        i_ng = 0
        
        for row in a_texts:
            if isinstance(row, str):
                # if row text is a single string
                a_ngrams = text_to_ngrams(
                    row,
                    i_max_size=self._max_size, i_min_size=self._min_size,
                    stop_words=self._stop_words, remove_punctuation=self._remove_punctuation,
                    b_case_insensitive=self._case_insensitive,
                )
            else:
                if isinstance(row[0], tuple):
                    # if row was already made into ngram-tuples
                    a_ngrams = row
                else:
                    # if row is tokenized text
                    a_ngrams = all_ngrams_tuples(row, self._max_size, self._min_size, self._stop_words)
            
            a_row_ngrams = []
            for ngram in a_ngrams:
                if ngram not in self.ngrams:
                    self.ngrams[ngram] = i_ng
                    i_ng += 1
                a_row_ngrams.append(self.ngrams[ngram])
                
            self.row_ngrams.append(a_row_ngrams)
        self._ngram_count = i_ng
    
    def _create_count_dicts(self):
        ''' Count all the ngrams in each category '''
        self._ngram_counts = {i_ng: {0: {0: 0}} for i_ng in range(self._ngram_count)}
        self._category_counts = {(i_field, i_cat): {}
                                 for i_field, e_field in self.categories.items()
                                 for i_cat in e_field.values()}
        
        # loop through rows and count ngrams in categories
        for a_ngs, a_cats in zip(self.row_ngrams, self.row_categories):
            for i_ng in a_ngs:
                e_ng = self._ngram_counts[i_ng]
                e_ng[0][0] += 1 # all field-all cat
                
                if i_ng not in self._category_counts[(0, 0)]:
                    self._category_counts[(0, 0)][i_ng] = 0
                self._category_counts[(0, 0)][i_ng] += 1
                    
                for i_field, i_cat in a_cats:
                    if i_field not in e_ng: e_ng[i_field] = {}
                    if i_cat not in e_ng[i_field]: e_ng[i_field][i_cat] = 0
                    e_ng[i_field][i_cat] += 1 # field-category
                    
                    if (i_field, i_cat) not in self._category_counts:
                        self._category_counts[(i_field, i_cat)] = {}
                    if i_ng not in self._category_counts[(i_field, i_cat)]:
                        self._category_counts[(i_field, i_cat)][i_ng] = 0
                    self._category_counts[(i_field, i_cat)][i_ng] += 1
                    
        # clear lists of ngrams and categories
        self.row_ngrams = []
        self.row_categories = []
    
    # update the dictionaries and counts
    def update(self, a_texts=[], a_fields=[], aa_categories=[]):
        
        # update indexes and turn new inputs into list of integers
        self._update_category_indexes(a_fields, aa_categories)
        self._update_ngram_indexes(a_texts)
        
        # count ngrams per category
        self._update_count_dicts()
        
    def _update_category_indexes(self, a_fields, aa_categories):
        ''' turn lists of categories into list of category_indexes, and
            create dicts of category: index
        '''
        i_old_fields = self._field_count
        a_new_fields = [x for x in a_fields if x not in self.field_names]
        self.field_names += a_new_fields
        self._field_count += len(a_new_fields)
        
        self.fields.update({field: idx + i_old_fields for idx, field in enumerate(a_new_fields)})
        self.idx_fields.update({idx + i_old_fields: field for idx, field in enumerate(a_new_fields)})
        
        self.categories.update({idx: {} for idx in self.idx_fields if not idx in self.categories})
        self.idx_categories.update({idx: {} for idx in self.idx_fields if not idx in self.idx_categories})
        
        i_category = self._category_count
        self.row_categories = []
        #ai_fields = [i for i in range(1, self._field_count)]
        ai_fields = [self.fields[x] for x in a_fields]
        
        for categories in zip(*aa_categories):
            a_row = []
            for i_field, cat in zip(ai_fields, categories):
                if cat not in self.categories[0]:
                    self.categories[0][cat] = i_category
                    self.idx_categories[0][i_category] = cat
                    i_category += 1
                a_row.append((0, self.categories[0][cat]))
                    
                if cat not in self.categories[i_field]:
                    self.categories[i_field][cat] = i_category
                    self.idx_categories[i_field][i_category] = cat
                    i_category += 1
                a_row.append((i_field, self.categories[i_field][cat]))
            
            self.row_categories.append(a_row)
        self._category_count = i_category
    
    def _update_ngram_indexes(self, a_texts):
        ''' turn list of texts into list of ngram_indexes, and
            create dict of ngram: index
        '''
        self.row_ngrams = []
        i_ng = self._ngram_count
        
        for row in a_texts:
            if isinstance(row, str):
                # if row text is a single string
                a_ngrams = text_to_ngrams(
                    row,
                    i_max_size=self._max_size, i_min_size=self._min_size,
                    stop_words=self._stop_words, remove_punctuation=self._remove_punctuation,
                    b_case_insensitive=self._case_insensitive,
                )
            else:
                if isinstance(row[0], tuple):
                    # if row was already made into ngram-tuples
                    a_ngrams = row
                else:
                    # if row is tokenized text
                    a_ngrams = all_ngrams_tuples(row, self._max_size, self._min_size, _stop_words)
            
            a_row_ngrams = []
            for ngram in a_ngrams:
                if ngram not in self.ngrams:
                    self.ngrams[ngram] = i_ng
                    i_ng += 1
                a_row_ngrams.append(self.ngrams[ngram])
                
            self.row_ngrams.append(a_row_ngrams)
        self._ngram_count = i_ng
    
    def _update_count_dicts(self):
        ''' Count all the ngrams in each category '''
        self._ngram_counts.update(
            {i_ng: {0: {0: 0}}
             for i_ng in range(len(self._ngram_counts), self._ngram_count)
             if i_ng not in self._ngram_counts}
        )
        self._category_counts.update(
            {(i_field, i_cat): {}
             for i_field, e_field in self.categories.items()
             for i_cat in e_field.values()
             if (i_field, i_cat) not in self._category_counts}
        )
        
        # loop through rows and count ngrams in categories
        for a_ngs, a_cats in zip(self.row_ngrams, self.row_categories):
            for i_ng in a_ngs:
                e_ng = self._ngram_counts[i_ng]
                e_ng[0][0] += 1 # all field-all cat
                
                if i_ng not in self._category_counts[(0, 0)]:
                    self._category_counts[(0, 0)][i_ng] = 0
                self._category_counts[(0, 0)][i_ng] += 1
                    
                for i_field, i_cat in a_cats:
                    if i_field not in e_ng: e_ng[i_field] = {}
                    if i_cat not in e_ng[i_field]: e_ng[i_field][i_cat] = 0
                    e_ng[i_field][i_cat] += 1 # field-category
                    
                    if (i_field, i_cat) not in self._category_counts:
                        self._category_counts[(i_field, i_cat)] = {}
                    if i_ng not in self._category_counts[(i_field, i_cat)]:
                        self._category_counts[(i_field, i_cat)][i_ng] = 0
                    self._category_counts[(i_field, i_cat)][i_ng] += 1
                    
        # clear lists of ngrams and categories
        self.row_ngrams = []
        self.row_categories = []
        
    # get labeled dictionaries out
    def field_dict(self, field='<all>'):
        ''' get a dict with labels for a specified field '''
        if field not in self.fields:
            print('field "{}" not in self.fields'.format(field))
            
        i_field = self.fields.get(field, 0)
        return {s_cat: {next(islice(self.ngrams.keys(), i_ng, None)): i_ct
                        for i_ng, i_ct in self._category_counts[(i_field, i_cat)].items()}
                for s_cat, i_cat in self.categories[i_field].items()}
    
    def category_dict(self, category='<all>', field='<all>'):
        ''' get a dict with labels for a specified category '''
        if field not in self.fields:
            print('field "{}" not in self.fields'.format(field), self.fields)
            return self.field_dict(field)
        
        i_field = self.fields[field]
        
        if category not in self.categories[i_field]:
            return self.field_dict(field)
        else:
            i_cat = self.categories[i_field][category]
            return {next(islice(self.ngrams.keys(), i_ng, None)): i_ct
                    for i_ng, i_ct in self._category_counts[(i_field, i_cat)].items()}
    
    def ngram_by_index(self, idx):
        ''' get an ngram label by index from the ordered dict keys'''
        try:
            return next(islice(self.ngrams.keys(), idx, None))
        except StopIteration:
            return None
    
    def ngram_total(self, ngram):
        ''' get the total appearances for an ngram '''
        i_ng = self.ngrams.get(ngram, None)
        if i_ng is None:
            if isinstance(ngram, int) and ngram >= 0 and ngram < self._ngram_count:
                i_ng = ngram
            else:
                return 0
        return self._ngram_counts[i_ng][0][0]
    
    def ngram_dict(self, ngram, field=''):
        ''' get the freq dict of category counts for an ngram '''
        i_ng = self.ngrams.get(ngram, None)
        if i_ng is None:
            if isinstance(ngram, int) and ngram >= 0 and ngram < self._ngram_count:
                i_ng = ngram
            else:
                return {}
        e_ng = self._ngram_counts[i_ng]
        
        if field in self.fields:
            i_field = self.fields[field]
        elif isinstance(field, int) and field in self.idx_fields:
            i_field = field
        else:            
            return {
                self.idx_fields[i_field]:
                {self.idx_categories[i_field][i_cat]: i_ct for i_cat, i_ct in e_cats.items()}
                for i_field, e_cats in e_ng.items()
            }
        
        return {
            self.idx_categories[i_field][i_cat]: i_ct
            for i_cat, i_ct in e_ng[i_field].items()
        }
                
                
    
    def ngram_count(self, ngram, category, field='<all>'):
        i_ng = self.ngrams.get(ngram, None)
        if i_ng is None:
            if isinstance(ngram, int) and ngram >= 0 and ngram < self._ngram_count:
                i_ng = ngram
            else:
                return 0
        
        if not category or category == '<all>':
            return self._ngram_counts[i_ng][0][0]
        elif field not in self.fields:
            field = '<all>'
        i_field = self.fields[field]
        
        if category in self.categories[i_field]:
            i_cat = self.categories[i_field][category]
            return self._ngram_counts[i_ng][i_field][i_cat]
        else:
            return 0
    
    def ngram_herfindahl(self, ngram, field='<all>'):
        i_ng = self.ngrams.get(ngram, None)
        if i_ng is None:
            if isinstance(ngram, int) and ngram >= 0 and ngram < self._ngram_count:
                i_ng = ngram
            else:
                return 0.0
        
        if not field or field == '<all>' or field not in self.fields:
            e_freqs = self._ngram_counts[i_ng][0].copy()
            del e_freqs[0]
        else:
            i_field = self.fields[field]
            e_freqs = self._ngram_counts[i_ng][i_field]
            
        return herfindahl_freqs_dict(e_freqs, b_ignoreblanks=False,)
