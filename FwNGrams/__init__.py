﻿import pyximport; pyximport.install()
try:
    from ngram_groups import *
except ImportError:
    from .ngram_groups import *

__version__ = "0.0.2"